﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VFXManager
{
    [System.Serializable]
    public class VFXData
    {
        public string name;
        public GameObject particlePrefab;
    }
}