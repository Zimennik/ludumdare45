﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace VFXManager
{
    public class VFXManager : MonoBehaviour
    {
        [Header("Main Character VFX")]
        [SerializeField] private VFXDataArray _mainCharacterVFXDataArray;

        [Header("Environment VFX")]
        [SerializeField] private VFXDataArray _enviromentVFXDataArray;

        private static VFXManager _instance;
        public static VFXManager Instance
        {
            get
            {
                return _instance;
            }
        }

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this);
            }
        }

        public void InstantiateVFX(string vfxName, Vector3 spawnPos, bool randomize = false)
        {
            GameObject finalVFX = _mainCharacterVFXDataArray.array.First(x => x.name == vfxName).particlePrefab;

            //if (randomize)
            //{
            //    RandomizeAndSpawnVFX(_audioSoundSource, finalSound);
            //}
            //else
            //{
            SpawnVFX(finalVFX, spawnPos);
            //}
        }

        public void InstantiateEnviromentVFX(string vfxName, Vector3 spawnPos, bool randomize = false)
        {
            GameObject finalVFX = _enviromentVFXDataArray.array.First(x => x.name == vfxName).particlePrefab;
            SpawnVFX(finalVFX, spawnPos);
        }

        public void SpawnVFX(GameObject particleToSpawn, Vector3 spawnPosition)
        {
            Instantiate(particleToSpawn, spawnPosition, Quaternion.identity);
        }
    }
}
