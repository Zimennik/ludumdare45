﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VFXManager
{
    [CreateAssetMenu(fileName = "VFX Data Array", menuName = "VFX/VFX Data Array")]
    public class VFXDataArray : ScriptableObject
    {
        public VFXData[] array;
    }
}