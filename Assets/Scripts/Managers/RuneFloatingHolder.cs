﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneFloatingHolder : MonoBehaviour
{
    public List<GameObject> Runes;

    public void SetRune(string rune)
    {
        switch (rune)
        {
            case "fire":
                Runes[1].SetActive(true);
                break;
            case "water":
                Runes[2].SetActive(true);
                break;
            case "wind":
                Runes[3].SetActive(true);
                break;
            case "stone":
                Runes[4].SetActive(true);
                break;
        }

        Runes[0].SetActive(false);
    }


    public void Reset()
    {
        foreach (GameObject rune in Runes)
        {
            rune.SetActive(false);
        }

        Runes[0].SetActive(true);
    }

    
}