﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunesVisualManager : MonoBehaviour
{
    [SerializeField] private List<RuneFloatingHolder> _runeHolders;

    private int index = 0;
    
    public void ActivateRune(int index, string rune)
    {
        _runeHolders[index].SetRune(rune);
    }

    
    public void SetRuneCount(int count)
    {
        
    }

    public void Reset()
    {
        
        foreach (RuneFloatingHolder runeFloatingHolder in _runeHolders)
        {
            runeFloatingHolder.Reset();
        }
    }
}