﻿using System.Collections;
using System.Collections.Generic;
using Helpers;
using Managers;
using Unit;
using UnityEngine;

namespace Unit.Controllers
{

    public class ElementsController : MonoBehaviour
    {
        [SerializeField] private float _timeForFullSlowMo = 0.5f;
        [SerializeField] private AnimationCurve _smoothCurve;
        [SerializeField] private FollowTargetPosition _mFollowCamera;
        private UnitBase _mCharacter;

        private float _holdMult;
        private bool _holdMode;

        void Start()
        {
            _mCharacter = GetComponent<UnitBase>();
        }

        public void HoldDown()
        {
            _holdMode = true;
            _mCharacter.GetElementsBundle().AllToVoid();
        }

        public void HoldUp()
        {
            _holdMode = false;
        }

        public void AddElements(int fire,int water,int earth, int air)
        {
            if (_holdMode)
            {
                _mCharacter.GetElementsBundle().AddStats(fire, water, earth, air);
            }
            
        }

        public void AddElements(bool fire, bool water, bool earth, bool air)
        {
            if (_holdMode)
            {
                _mCharacter.GetElementsBundle().AddStats(fire?1:0, water ? 1 : 0, earth ? 1 : 0, air ? 1 : 0);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (_holdMode)
            {
                if (_holdMult > 0)
                {
                    _holdMult -= Time.deltaTime / _timeForFullSlowMo;

                    TimeManager.SetTimeMult(0.1f+ 0.9f* _smoothCurve.Evaluate(_holdMult));
                    _mFollowCamera._posOffsetMult = (0.5f + 0.5f * _smoothCurve.Evaluate(_holdMult));

                    if (_holdMult < 0)
                    {
                        _holdMult = 0;
                    }
                }
            }
            else
            {
                if (_holdMult < 1)
                {
                    _holdMult += 3 * Time.deltaTime / _timeForFullSlowMo;

                    TimeManager.SetTimeMult(0.2f + 0.8f * _smoothCurve.Evaluate(_holdMult));
                    _mFollowCamera._posOffsetMult = (0.5f + 0.5f * _smoothCurve.Evaluate(_holdMult));

                    if (_holdMult > 1)
                    {
                        _holdMult = 1;
                    }
                }
            }
        }
    }
}
