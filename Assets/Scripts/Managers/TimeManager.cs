﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Managers
{
    public class TimeManager
    {
        public static float timeMult = 1;
        public static void SetTimeMult(float mult = 1)
        {
            timeMult = mult;
            Time.timeScale = 1.45f * timeMult;
            Time.fixedDeltaTime = Mathf.Clamp(0.02f * timeMult, 0.001f,0.02f) ;
        }
    }
}

