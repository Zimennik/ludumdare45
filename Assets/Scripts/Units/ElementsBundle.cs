﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit.Bundles
{
    [Serializable]
    public class ElementsBundle
    {
        public int Fire;
        public int Water;
        public int Earth;
        public int Air;
        public int Void;

        ElementsBundle(int fire, int water, int earth, int air, int pVoid = 0)
        {
            Fire = fire;
            Water = water;
            Earth = earth;
            Air = air;
            Void = pVoid;
        }

        public void AddStats(int fire, int water, int earth, int air)
        {
            Fire += Void > 0 ? fire : 0;
            Water += Void > 0 ? water : 0;
            Earth += Void > 0 ? earth : 0;
            Air += Void > 0 ? air : 0;

            Void = Void - fire - water - earth - air;
            if (Void < 0)
            {
                Void = 0;
            }
        }

        public void AddVoid()
        {
            Void++;
        }

        public void AllToVoid()
        {
            Void = Void + Fire + Water + Earth + Air;

            Fire = 0;
            Water = 0;
            Earth = 0;
            Air = 0;
        }
    }
}
