﻿using System;
using System.Collections;
using System.Collections.Generic;
using Helpers;
using Unit.Bundles;
using UnityEngine;

namespace Unit
{
    public class UnitBase : MonoBehaviour
    {
        [SerializeField] private ElementsBundle _mainElementsBundle;
        [SerializeField] private CalibrateStatsBundle _mainConstantStats;
        [SerializeField] private GameObject _fireballPrefab;

        private float _healthPoint;

        private Rigidbody2D _mRigidbody2D;
        private bool _onFloor;
        private bool _tryJump;

        /// <summary>
        /// Look right - 1, left = -1
        /// </summary>
        private int _sideLook = 1;
        private float _movementSideMove;


        // Start is called before the first frame update
        void Start()
        {
            _mRigidbody2D = GetComponent<Rigidbody2D>();
            _healthPoint = _mainConstantStats.MaxHealth;
        }

        #region Controls method

        public void SetMoveSide(int side)
        {
            _movementSideMove = _movementSideMove + Mathf.Clamp((float)side - _movementSideMove,-_mainConstantStats.AccelerationViaController * Time.deltaTime, _mainConstantStats.AccelerationViaController * Time.deltaTime);
            _sideLook = side != 0 ? side : _sideLook;
        }

        public void Jump(bool newJump = false)
        {
            _tryJump = true;
            //Debug.Log($"{_onFloor} {_jumpCounts} {_mainConstantStats.MaxJumps}" );
            if (_onFloor && newJump)
            {
                _onFloor = false;
                //h = s^2/20 >>> s = sqrt(20h)
                //
                //Debug.Log(_mRigidbody2D.velocity +  " tttt");
                _mRigidbody2D.velocity = new Vector2(_mRigidbody2D.velocity.x,
                    (Mathf.Sqrt(Mathf.Clamp(_mainElementsBundle.Water - _mainElementsBundle.Earth, 0, 4) * 20 *
                                _mainConstantStats.JumpMult)));
                //Debug.Log(_mRigidbody2D.velocity);
            }
        }

        public void Attack()
        {
            CreateFireBall();
        }

        public void TakeDamage()
        {
            _healthPoint -= 1;
            if (_healthPoint == 0)
            {
                Death();
                _healthPoint = _mainConstantStats.MaxHealth;
            }
        }

        #endregion

        #region Public method for logic

        public void SetOnFloorState(bool state)
        {
            _onFloor = state;
        }

        public ElementsBundle GetElementsBundle()
        {
            return _mainElementsBundle;
        }

        #endregion

        #region CreateInstance

        private void CreateFireBall()
        {
            GameObject fireBall = Instantiate(_fireballPrefab);
            fireBall.transform.position = transform.position + Vector3.right* _sideLook;
            fireBall.GetComponent<Fireball>().InitFireball(_sideLook, _mainElementsBundle.Fire);
        }

        #endregion


        private void Death()
        {

        }

        // Update is called once per frame
        void Update()
        {
            int returnY = _onFloor ? 0 : 1;

            //(_sideLook* _sideLook- _sideLook) >> [_sideLook == 1] => "(1*1-1)=0" // [_sideLook == -1] => "(-1*-1--1)=2"
            transform.rotation = Quaternion.Euler(0, (_sideLook* _sideLook- _sideLook)*90, 0);

            float multSpeed = (1 + _mainElementsBundle.Air - _mainElementsBundle.Earth);
            if (multSpeed < 1)
            {
                multSpeed = 1.0f / (_mainElementsBundle.Earth);
            }

            multSpeed = _movementSideMove * (multSpeed + Mathf.Clamp(_mainConstantStats.SpeedAdd - 1, -1f, _mainConstantStats.SpeedAdd)) * _mainConstantStats.SpeedMult;

            if (!_onFloor && (Mathf.Sign(multSpeed) * Mathf.Sign(_mRigidbody2D.velocity.x) > 0 || Mathf.Abs(_movementSideMove) < 0.1f))
            {
                if (Mathf.Abs(multSpeed) < Mathf.Abs(_mRigidbody2D.velocity.x))
                {
                    multSpeed = _mRigidbody2D.velocity.x;
                }
            }

            _mRigidbody2D.velocity = Vector2.up * _mRigidbody2D.velocity.y * returnY + Vector2.right * multSpeed;
          

            if (_mRigidbody2D.velocity.y < 0)
            {
                _mRigidbody2D.velocity += Vector2.up*Physics2D.gravity.y*1.5f*Time.deltaTime;
            }
            else if(_mRigidbody2D.velocity.y > 0 && !_tryJump)
            {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            }

            _tryJump = false;
            //_onFloor = false;
        }
    }
}
