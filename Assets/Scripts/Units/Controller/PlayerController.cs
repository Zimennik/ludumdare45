﻿using System.Collections;
using System.Collections.Generic;
using Helpers;
using Managers;
using UnityEngine;

namespace Unit.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        private UnitBase _mUnit;
        private ElementsController _mElements;

        public bool BlockControl;
        // Start is called before the first frame update
        void Start()
        {
            _mUnit = GetComponent<UnitBase>();
            _mElements = GetComponent<ElementsController>();
            if (_mUnit == null || _mElements == null)
            {
                Destroy(this);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (BlockControl)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                _mElements.HoldDown();
            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                _mElements.HoldUp();
            }

            if (Input.GetKey(KeyCode.Space))
            {
                _mElements.AddElements(Input.GetKeyDown(KeyCode.UpArrow),
                    Input.GetKeyDown(KeyCode.RightArrow),
                    Input.GetKeyDown(KeyCode.DownArrow),
                    Input.GetKeyDown(KeyCode.LeftArrow));

                _mUnit.SetMoveSide(0);
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    _mUnit.SetMoveSide(-1);
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    _mUnit.SetMoveSide(1);
                }
                else
                {
                    _mUnit.SetMoveSide(0);
                }

                if (Input.GetKeyDown(KeyCode.Z))
                {
                    _mUnit.Jump(true);
                }

                if (Input.GetKey(KeyCode.Z))
                {
                    _mUnit.Jump();
                }

                if (Input.GetKeyDown(KeyCode.X))
                {
                    _mUnit.Attack();
                }
            }


        }
    }
}
