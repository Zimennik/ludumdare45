﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private float _speed;

    private float _dist = 100;
    private int _side;
    private float _startLocalPos = 0;

    public void InitFireball(int side,float dist)
    {
        _side = side;
        _dist = dist;
        _startLocalPos = transform.position.x;
        if (dist == 0)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right* _side* _speed*Time.deltaTime*Time.timeScale);
        if (Mathf.Abs(transform.position.x - _startLocalPos) > _dist)
        {
            DestroyViaCollision();
        }
    }

    private void DestroyViaCollision()
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Destroy(gameObject);
    }
}
