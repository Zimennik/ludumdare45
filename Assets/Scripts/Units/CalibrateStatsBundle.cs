﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit.Bundles
{
    [Serializable]
    public class CalibrateStatsBundle
    {
        public float AccelerationViaController;
        public float JumpMult;
        public float SpeedMult;
        public float SpeedAdd;
        public float MaxHealth;
    }
}

