﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleTrigger : MonoBehaviour
{
    public UnityEvent OnTriggerEnter = new UnityEvent();
    public UnityEvent OnTriggerExit = new UnityEvent();
    public UnityEvent OnTriggerStay = new UnityEvent();

    void OnTriggerEnter2D(Collider2D other)
    {

        OnTriggerEnter?.Invoke();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        
        OnTriggerStay?.Invoke();
    }

    void OnTriggerExit2D(Collider2D other)
    {
        
        OnTriggerExit?.Invoke();
    }
}
