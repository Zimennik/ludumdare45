﻿using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

namespace Helpers
{
    public class FollowTargetPosition : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private Rigidbody2D _targetRB;
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _maxAcceleration;
        [SerializeField] private Transform _pivot;
        [SerializeField] private Transform _camera;
        [SerializeField] private Vector3 _posOffset;
        [SerializeField] private float _rotateMult = 1;
        [SerializeField] private float _smoothFactor = 1;
        [SerializeField] private bool _justSetPos;

        private float _oldSpeed;
        private float _smoothSpeed;

        public float _posOffsetMult = 1;
        [SerializeField] private AnimationCurve _smoothCurve;

        //Ex order - 95
        void FixedUpdate()
        {
            
            //_target.position;

            transform.position = Vector3.Lerp(transform.position, _target.position, Time.fixedDeltaTime * 3);

            if (!_justSetPos)
            {

                if (_camera != null)
                {
                    _camera.localPosition = _posOffset * _posOffsetMult * (0.5f + 0.5f * TimeManager.timeMult);
                }


                float step = _smoothFactor * Time.fixedDeltaTime * Time.timeScale;


                _smoothSpeed = _smoothSpeed + Mathf.Clamp(_targetRB.velocity.x - _smoothSpeed, -step, step);

                _oldSpeed = _targetRB.velocity.x;

                if (_targetRB != null && _pivot != null)
                {
                    _pivot.rotation = Quaternion.Euler(Vector3.up * _rotateMult *
                                                       _smoothCurve.Evaluate(Mathf.Clamp(_smoothSpeed / _maxSpeed, -1,
                                                           1)));

                }
            }


        }
    }
}

