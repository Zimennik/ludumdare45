using UnityEngine;
using System.Collections.Generic;

namespace Helpers
{
    public class SmoothCamera : MonoBehaviour
    {

        [SerializeField] private int _smoothingFrames = 2;

        //[SerializeField] private float _maxDistance;
        // private Quaternion smoothedRotation;
        private Vector3 smoothedPosition;

       // private Queue<Quaternion> rotations;
        private Queue<Vector3> positions;

        //Ex order - 95
        void Update()
        {

            if (positions.Count >= _smoothingFrames)
            {
               // rotations.Dequeue();
                positions.Dequeue();
            }

            //rotations.Enqueue(transform.rotation);
            positions.Enqueue(transform.position);

            Vector4 avgr = Vector4.zero;
            //foreach (Quaternion singleRotation in rotations)
            //{
            //    Math3d.AverageQuaternion(ref avgr, singleRotation, rotations.Peek(), rotations.Count);
            //}

            Vector3 avgp = Vector3.zero;
            foreach (Vector3 singlePosition in positions)
            {
                avgp += singlePosition;
            }
            avgp /= positions.Count;

            //smoothedRotation = new Quaternion(avgr.x, avgr.y, avgr.z, avgr.w);
            smoothedPosition = avgp;
        }

        // Use this for initialization
        void Start()
        {

            //rotations = new Queue<Quaternion>(_smoothingFrames);
            positions = new Queue<Vector3>(_smoothingFrames);
        }

        // Update is called once per frame
        void LateUpdate()
        {
           

            //transform.rotation = smoothedRotation;
            transform.position = smoothedPosition;
        }

    }

}
