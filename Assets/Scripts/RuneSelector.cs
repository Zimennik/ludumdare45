﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneSelector : MonoBehaviour
{
    [SerializeField] private Sprite _water;
    [SerializeField] private Sprite _fire;
    [SerializeField] private Sprite _earth;
    [SerializeField] private Sprite _wind;

    [SerializeField] private Sprite _empty;

    [SerializeField] private List<Image> runes;


    public void SetRune(string rune, int order)
    {
        Sprite sprite = _empty;

        switch (rune)
        {
            case "water":
                sprite = _water;
                break;
            case "fire":
                sprite = _fire;
                break;
            case "stone":
                sprite = _earth;
                break;
            case "air":
                sprite = _wind;
                break;
        }

        runes[order].sprite = sprite;
    }


    public void ResetAll()
    {
        foreach (Image image in runes)
        {
            image.sprite = _empty;
        }
    }

    public void SetRuneCount(int count)
    {
        int i = 0;
        foreach (Image image in runes)
        {
            image.gameObject.SetActive(i < count);
            i++;
        }
    }
}