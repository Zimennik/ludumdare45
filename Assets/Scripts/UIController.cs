﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public RuneSelector _runeSelector;


    public void ShowAllUI()
    {
        gameObject.SetActive(true);
    }

    public void HideAllUI()
    {
        gameObject.SetActive(false);
    }

    public void ShowRuneSelect()
    {
        _runeSelector.gameObject.SetActive(true);
    }

    public void HideRuneSelect()
    {
        _runeSelector.gameObject.SetActive(false);
    }
}