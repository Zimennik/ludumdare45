﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomOffset : MonoBehaviour
{
    [SerializeField] private Vector3 _minOffset;
    [SerializeField] private Vector3 _maxOffset;
    [SerializeField] private float _multiply;

    private Vector3 _mainPos;

    // Start is called before the first frame update
    void Start()
    {
        _mainPos = transform.localPosition;
    }

    public void SetMainPos(Vector3 pos)
    {
        _mainPos = pos;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = _mainPos + new Vector3(Random.Range(_minOffset.x, _maxOffset.x),
                                      Random.Range(_minOffset.y, _maxOffset.y),
                                      Random.Range(_minOffset.z, _maxOffset.z)) * _multiply;
    }
}
