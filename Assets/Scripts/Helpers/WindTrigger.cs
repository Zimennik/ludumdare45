﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindTrigger : MonoBehaviour
{
    [SerializeField] private float _distance;
    [SerializeField] private float _distanceMult;
    [SerializeField] private float _addSpeed;

    private Rigidbody2D _targetRigidbody;
    private Vector2 _velocityAdd;

    void OnTriggerStay2D(Collider2D other)
    {
        _targetRigidbody = other.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (_targetRigidbody != null)
        {
            float delta = _targetRigidbody.transform.position.x - transform.position.x;
            _targetRigidbody.velocity += Vector2.right * _addSpeed * Mathf.Sign(delta) * Mathf.Clamp01((_distance - Mathf.Abs(delta)) * _distanceMult / _distance);

            _targetRigidbody = null;
        }
        
        
    }
}
