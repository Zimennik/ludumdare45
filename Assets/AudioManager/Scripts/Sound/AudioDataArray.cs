﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AudioManager
{
    [CreateAssetMenu(fileName = "Audio Data Array", menuName = "Audio/Audio Data Array")]
    public class AudioDataArray : ScriptableObject
    {
        public AudioData[] array;
    }
}
