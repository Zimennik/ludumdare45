﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AudioManager
{
    public class TestAudio : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
                AudioManager.Instance.PlaySoundMainCharacter("1", true);
            if (Input.GetKeyDown(KeyCode.S))
                AudioManager.Instance.PlaySoundMainCharacter("2", true);
            if (Input.GetKeyDown(KeyCode.D))
                AudioManager.Instance.PlaySoundMainCharacter("3", true);
            if (Input.GetKeyDown(KeyCode.Q))
                AudioManager.Instance.PlaySoundEnviroment("4", Vector3.zero);
            if (Input.GetKeyDown(KeyCode.W))
                AudioManager.Instance.PlaySoundEnviroment("5", Vector3.one);
            if (Input.GetKeyDown(KeyCode.E))
                AudioManager.Instance.PlaySoundEnviroment("6", Vector3.zero);
            if (Input.GetKeyDown(KeyCode.Z))
                AudioManager.Instance.PlayBackroundMusic("1", true);
            if (Input.GetKeyDown(KeyCode.X))
                AudioManager.Instance.PlayBackroundMusic("2", true);
            if (Input.GetKeyDown(KeyCode.C))
                AudioManager.Instance.PlayBackroundMusic("3", true);
        }
    }
}
