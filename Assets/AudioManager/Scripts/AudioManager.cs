﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AudioManager
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private float _lowPitchRange = 0.95f;              //The lowest a sound effect will be randomly pitched.
        [SerializeField] private float _highPitchRange = 1.05f;

        [Header("Sounds main character")]
        [SerializeField] private AudioDataArray _audioSoundMainCharacterDataArray;
        [SerializeField] private AudioSource _audioSoundSource;

        [Header("Sounds enviroment")]
        [SerializeField] private AudioDataArray _audioSoundEnviromentDataArray;

        [Header("Background music")]
        [SerializeField] private AudioDataArray _audioBackgroundMusicDataArray;
        [SerializeField] private AudioSource _audioBackgroundMusicSource;

        private static AudioManager _instance;
        public static AudioManager Instance
        {
            get
            {
                return _instance;
            }
        }

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this);
            }
        }

        public void PlaySoundMainCharacter(string audioName, bool randomize = false)
        {
            AudioClip finalSound = _audioSoundMainCharacterDataArray.array.First(x => x.name == audioName).clip;

            if (randomize)
                RandomizeSfx(_audioSoundSource, finalSound);
            else
            {
                SimplePlay(_audioSoundSource, finalSound);
            }
        }

        public void PlaySoundEnviroment(string audioName, Vector3 audioPosition, bool randomize = false)
        {
            AudioClip finalSound = _audioSoundEnviromentDataArray.array.First(x => x.name == audioName).clip;

            AudioSource.PlayClipAtPoint(finalSound, audioPosition);
        }

        public void PlayBackroundMusic(string audioName, bool randomize = false)
        {
            AudioClip finalMusic = _audioBackgroundMusicDataArray.array.First(x => x.name == audioName).clip;

            if (randomize)
                RandomizeSfx(_audioBackgroundMusicSource, finalMusic);
            else
            {
                SimplePlay(_audioBackgroundMusicSource, finalMusic);
            }
        }

        public void StopBackroundMusic()
        {
            _audioBackgroundMusicSource.Stop();
        }

        private void RandomizeSfx(AudioSource audioSource, AudioClip clip)
        {
            float randomPitch = Random.Range(_lowPitchRange, _highPitchRange);

            audioSource.pitch = randomPitch;
            audioSource.clip = clip;
            audioSource.Play();
        }

        private void SimplePlay(AudioSource audioSource, AudioClip clip)
        {
            audioSource.pitch = 1;
            audioSource.clip = clip;
            audioSource.Play();
        }
    }
}
