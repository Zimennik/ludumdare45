﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class FirePillowInput : EnviromentTriggerAction
    {        
        [SerializeField] private int FireCountToActivate;

        public override void СonditionToActivate(GameObject bufGameObject)
        {
            //Unit.UnitBase bufUnit = bufGameObject.GetComponent<Unit.UnitBase>();

            //if (bufUnit != null)
            //{
            //    bool canActivate = bufUnit.GetElementsBundle().Fire >=
            //        FireCountToActivate;

            if (bufGameObject.tag == "Fireball")
            {
                ActivateAction();
            }
        }
    //}

        public override void СonditionToDectivate(GameObject bufGameObject)
        {
            //Unit.UnitBase bufUnit = bufGameObject.GetComponent<Unit.UnitBase>();

            //if (bufUnit != null)
            //{
            //    bool canDeactivate = bufUnit.GetElementsBundle().Fire >=
            //        FireCountToActivate;

            if (bufGameObject.tag == "Fireball")
            {
                DeactivateAction();
            }
        }
    }
}
