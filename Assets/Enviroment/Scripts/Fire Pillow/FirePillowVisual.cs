﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class FirePillowVisual : MonoBehaviour, IEnviromentVisual
    {
        [SerializeField] private ParticleSystem[] _fireParticles;

        void Awake()
        {
            Reset();
        }

        public void Reset()
        {
            VisualDeactivate();
        }

        public void VisualActivate()
        {
            SetActiveFireParticles(true);
        }

        public void VisualDeactivate()
        {
            SetActiveFireParticles(false);
        }

        private void SetActiveFireParticles(bool value)
        {
            if(value)
            {
                for (int i = 0; i < _fireParticles.Length; i++)
                {
                    _fireParticles[i].Play();
                }
            }
            else
            {
                for (int i = 0; i < _fireParticles.Length; i++)
                {
                    _fireParticles[i].Stop();
                }
            }
        }
    }
}
