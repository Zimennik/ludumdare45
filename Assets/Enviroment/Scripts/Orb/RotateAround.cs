﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class RotateAround : MonoBehaviour
    {
        [SerializeField] private float _speed;

        void Start()
        {
            StartCoroutine(Rotation());
        }

        private IEnumerator Rotation()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();
                transform.RotateAround(transform.position, transform.up, Time.deltaTime * _speed);
            }
        }
    }
}
