﻿using Core;
using System.Collections;
using System.Collections.Generic;
using Unit;
using UnityEngine;

namespace Enviroment
{
    public class Orb : MonoBehaviour, IResetable
    {
        public void Reset()
        {
            gameObject.SetActive(true);
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            collider.GetComponent<UnitMain>().AddEmptyElement();

            gameObject.SetActive(false);
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            
        }
    }
}
