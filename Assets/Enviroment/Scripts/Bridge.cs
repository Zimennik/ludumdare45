﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class Bridge : MonoBehaviour
    {
        [SerializeField] private Color _baseColor;

        private MeshRenderer _bridgeMesh;
        private Collider2D _collider;
        private Material _mat;

        private void Awake()
        {
            _bridgeMesh = GetComponent<MeshRenderer>();
            _collider = GetComponent<Collider2D>();
            _mat = _bridgeMesh.material;
        }

        private Unit.UnitMain _player;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var player = collision.gameObject.GetComponent<Unit.UnitMain>();

            if (player == null) return;

            _player = player;

            bool canSmash = player.GetStateID() == "Smash";
            if (canSmash)
            {
                SmashBridge();
            }
        }

        public void SmashBridge()
        {
            StartCoroutine(DestroyBridge());
        }

        private IEnumerator DestroyBridge()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                float emission = Mathf.PingPong(Time.time, 1f);

                Color finalColor = _baseColor * Mathf.LinearToGammaSpace(emission);

                _mat.SetColor("_EmissionColor", finalColor);

                if (emission > 0.9f)
                    break;

                //yield return new WaitForSeconds(0.5f);
            }

            gameObject.SetActive(false);

            if (_player != null)
            {
                _player.SetSpacialAbility();
                _player.SetOnFloor(false);
            }
        }
    }
}