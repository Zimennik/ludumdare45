﻿using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class Teleport : MonoBehaviour
    {
        public Transform respawnPoint;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var unit = collision.gameObject.GetComponent<IUnit>();

            //if (unit != null)
            //{
                collision.transform.position = respawnPoint.position;     
            //}
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            //collision.gameObject.GetComponent<IUnit>().TakeDamage(1f);
        }
    }
}
