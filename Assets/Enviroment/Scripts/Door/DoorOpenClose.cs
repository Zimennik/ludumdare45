﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Enviroment
{
    public class DoorOpenClose : MonoBehaviour, IResetable
    {
        public bool defaultState;

        [Range(0.001f, 2f)]
        [SerializeField] private float _openSpeed;

        [Range(0.001f, 2f)]
        [SerializeField] private float _closeSpeed;

        [SerializeField] private Collider2D _collider;

        private IDoorVisual _doorVisual;

        public void Reset()
        {
            _doorVisual = GetComponent<IDoorVisual>();

            if(_doorVisual!=null)
            {
                _doorVisual.DefaultState = defaultState;
                _doorVisual.ResetVisual();
            }
            
            SetActiveDoorState(defaultState);
        }

        private void OnEnable()
        {
            EnviromentPropsInputAction.OnActivate += OpenDoor;
            EnviromentPropsInputAction.OnDeactivate += CloseDoor;
        }

        private void OnDisable()
        {
            EnviromentPropsInputAction.OnActivate -= OpenDoor;
            EnviromentPropsInputAction.OnDeactivate -= CloseDoor;
        }

        public void OpenDoor(GameObject[] bufGameObjects)
        {
            if (bufGameObjects.Contains(gameObject))
            {
                SetActiveDoorState(true);
                _doorVisual?.OpenDoor(_openSpeed, () => Debug.Log("Finish Open"));
            }
        }

        public void CloseDoor(GameObject[] bufGameObjects)
        {
            if (bufGameObjects.Contains(gameObject))
            {
                SetActiveDoorState(false);
                _doorVisual?.CloseDoor(_closeSpeed, () => Debug.Log("Finish Close"));
            }
        }

        private void SetActiveDoorState(bool value)
        {
            if(_collider != null)
                _collider.isTrigger = value;
        }
    }
}
