﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class DoorCageVisual : MonoBehaviour, IDoorVisual
    {
        [SerializeField] private Transform _cageGridClosed;
        [SerializeField] private float _closedHeight = 5.37f;

        private bool _defaultState;
        public bool DefaultState { get { return _defaultState; } set { _defaultState = value; } }

        private Coroutine _openDoorCoroutine;
        private Coroutine _closeDoorCoroutine;
        private float y;

        public void ResetVisual()
        {
            if(!DefaultState)
            {
                _cageGridClosed.localPosition = Vector3.zero;
            }
            else
            {
                _cageGridClosed.localPosition = new Vector3(0f, _closedHeight, 0f);
            }
        }

        public void CloseDoor(float speed, Action afterClose)
        {
            CheckAndStopPrevCoroutine();

            _closeDoorCoroutine = StartCoroutine(CloseDoorCoroutine(speed, afterClose));
        }

        public void OpenDoor(float speed, Action afterOpen)
        {
            CheckAndStopPrevCoroutine();

            _openDoorCoroutine = StartCoroutine(OpenDoorCoroutine(speed, afterOpen));
        }

        private void CheckAndStopPrevCoroutine()
        {
            if (_openDoorCoroutine != null)
                StopCoroutine(_openDoorCoroutine);

            if (_closeDoorCoroutine != null)
                StopCoroutine(_closeDoorCoroutine);
        }

        private IEnumerator OpenDoorCoroutine(float speed, Action afterOpen)
        {
            y = _cageGridClosed.localPosition.y;
            float startTime = Time.time;

            while (_cageGridClosed.localPosition.y < _closedHeight)
            {
                Vector3 bufPosition = _cageGridClosed.localPosition;

                y += 0.05f;

                yield return new WaitForSeconds(speed * Time.deltaTime);

                Vector3 destination = new Vector3(bufPosition.x, y, bufPosition.z);

                _cageGridClosed.localPosition = destination;
            }

            afterOpen?.Invoke();
        }

        private IEnumerator CloseDoorCoroutine(float speed, Action afterClose)
        {
            y = _cageGridClosed.localPosition.y;

            while (_cageGridClosed.localPosition.y > 0f)
            {
                Vector3 bufPosition = _cageGridClosed.localPosition;

                y -= 0.05f;

                yield return new WaitForSeconds(speed * Time.deltaTime);

                Vector3 destination = new Vector3(bufPosition.x, y, bufPosition.z);

                _cageGridClosed.localPosition = destination;
            }

            afterClose?.Invoke();
        }
    }
}
