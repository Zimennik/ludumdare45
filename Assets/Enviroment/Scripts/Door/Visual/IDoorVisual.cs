﻿using System;

public interface IDoorVisual
{
    bool DefaultState { get; set; }
    void OpenDoor(float speed, Action afterOpen);
    void CloseDoor(float speed, Action afterClose);
    void ResetVisual();
}
