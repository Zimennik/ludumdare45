﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class DoorSimpleVisual : MonoBehaviour, IDoorVisual
    {
        [SerializeField] private Transform _doorHolder;
        [SerializeField] private float _angleOpenDoor = -100f;

        private bool _defaultState;
        public bool DefaultState { get { return _defaultState; } set { _defaultState = value; } }

        private Coroutine _openDoorCoroutine;
        private Coroutine _closeDoorCoroutine;
        private float y;

        public void ResetVisual()
        {
            if (!DefaultState)
            {
                _doorHolder.localEulerAngles = Vector3.zero;
            }
            else
            {
                _doorHolder.localEulerAngles = new Vector3(0f, _angleOpenDoor, 0f);
            }
            
        }

        public void CloseDoor(float speed, Action afterClose)
        {
            CheckAndStopPrevCoroutine();

            _closeDoorCoroutine = StartCoroutine(CloseDoorCoroutine(speed, afterClose));
        }

        public void OpenDoor(float speed, Action afterOpen)
        {
            CheckAndStopPrevCoroutine();

            _openDoorCoroutine = StartCoroutine(OpenDoorCoroutine(speed, afterOpen));
        }

        private void CheckAndStopPrevCoroutine()
        {
            if (_openDoorCoroutine != null)
                StopCoroutine(_openDoorCoroutine);

            if (_closeDoorCoroutine != null)
                StopCoroutine(_closeDoorCoroutine);
        }

        private IEnumerator OpenDoorCoroutine(float speed, Action afterOpen)
        {
            float angleLimit = Mathf.Abs(_angleOpenDoor);
            int direction = (_angleOpenDoor < 0) ? (-1) : 1;
            float updateSeconds = speed * 0.01f;

            while (Mathf.Abs(y) < angleLimit)
            {
                Vector3 bufAngle = _doorHolder.localEulerAngles;
                y = bufAngle.y + 1f * (direction);

                y = (y > 180f) ? y - 360f : y;

                Debug.Log(y);

                yield return new WaitForSeconds(updateSeconds);

                _doorHolder.localEulerAngles = new Vector3(bufAngle.x, y, bufAngle.z);
            }

            afterOpen?.Invoke();
        }

        private IEnumerator CloseDoorCoroutine(float speed, Action afterClose)
        {
            float angleLimit = Mathf.Abs(_angleOpenDoor);
            int direction = (_angleOpenDoor < 0) ? (-1) : 1;
            float updateSeconds = speed * 0.01f;

            while (Mathf.Abs(y) > 0)
            {
                Vector3 bufAngle = _doorHolder.localEulerAngles;
                y = bufAngle.y - 1f * (direction);

                y = (y > 180f) ? y - 360f : y;

                Debug.Log(y);

                yield return new WaitForSeconds(updateSeconds);

                _doorHolder.localEulerAngles = new Vector3(bufAngle.x, y, bufAngle.z);
            }

            afterClose?.Invoke();
        }
    }
}
