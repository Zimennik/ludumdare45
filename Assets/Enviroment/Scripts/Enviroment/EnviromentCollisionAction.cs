﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class EnviromentCollisionAction : EnviromentPropsInputAction, IResetable
    {
        [SerializeField] private GameObject[] _linkedObject;
        [SerializeField] private bool _activateOnce;

        private IEnviromentVisual _enviromentVisual;
        private bool _isActivated;

        private void Start()
        {
            _enviromentVisual = GetComponent<IEnviromentVisual>();
        }

        public virtual void Reset()
        {
            _isActivated = false;
        }

        public virtual void СonditionToActivate(GameObject bufGameObject)
        {
            ActivateAction();
        }

        public virtual void СonditionToDectivate(GameObject bufGameObject)
        {
            DeactivateAction();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            СonditionToActivate(collision.gameObject);
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            СonditionToDectivate(collision.gameObject);
        }

        protected void ActivateAction()
        {
           // if (!_activateOnce || (_activateOnce && !_isActivated))
           // {
                if (_enviromentVisual != null)
                    _enviromentVisual.VisualActivate();

                if (OnActivate != null)
                    OnActivate(_linkedObject);

                _isActivated = true;
            //}
        }

        protected void DeactivateAction()
        {
            if (!_activateOnce || (_activateOnce && !_isActivated))
            {
                if (_enviromentVisual != null)
                    _enviromentVisual.VisualDeactivate();

                if (OnDeactivate != null)
                    OnDeactivate(_linkedObject);
            }
        }
    }
}
