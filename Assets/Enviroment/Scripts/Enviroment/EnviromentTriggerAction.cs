﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class EnviromentTriggerAction : EnviromentPropsInputAction, IResetable
    {
        [SerializeField] private GameObject[] _linkedObject;
        [SerializeField] private bool _activateOnce;

        private IEnviromentVisual _enviromentVisual;
        private bool _isActivated;

        private void Start()
        {
            _enviromentVisual = GetComponent<IEnviromentVisual>();
        }

        public virtual void Reset()
        {
            _isActivated = false;
        }

        public virtual void СonditionToActivate(GameObject bufGameObject)
        {
            ActivateAction();
        }

        public virtual void СonditionToDectivate(GameObject bufGameObject)
        {
            DeactivateAction();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            СonditionToActivate(collider.gameObject);
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            СonditionToDectivate(collider.gameObject);
        }

        protected void ActivateAction()
        {
            //if (!_activateOnce || (_activateOnce && !_isActivated))
            //{
                if (_enviromentVisual != null)
                    _enviromentVisual.VisualActivate();

                if (OnActivate != null)
                    OnActivate(_linkedObject);

                _isActivated = true;
            //}
        }

        protected void DeactivateAction()
        {
            if (!_activateOnce || (_activateOnce && !_isActivated))
            {
                if (_enviromentVisual != null)
                    _enviromentVisual.VisualDeactivate();

                if (OnDeactivate != null)
                    OnDeactivate(_linkedObject);

                _isActivated = false;
            }
        }
    }
}
