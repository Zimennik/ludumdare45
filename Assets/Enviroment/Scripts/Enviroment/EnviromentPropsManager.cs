﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Enviroment
{
    public class EnviromentPropsManager : MonoBehaviour
    {
        private Coroutine _resetCoroutine;

        private void Start()
        {
            GlobalResetProps();
        }

        void GlobalResetProps()
        {
            List<IResetable> resetList = FindObjectsOfType<MonoBehaviour>().OfType<IResetable>().ToList();

            if (_resetCoroutine != null)
                StopCoroutine(_resetCoroutine);

            _resetCoroutine = StartCoroutine(ResetCoroutine(resetList));
        }

        private IEnumerator ResetCoroutine(List<IResetable> resetList)
        {
            foreach (IResetable reset in resetList)
            {
                yield return new WaitForEndOfFrame();
                reset.Reset();
            }
        }
    }
}
