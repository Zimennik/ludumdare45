﻿
namespace Enviroment
{
    public interface IResetable
    {
        void Reset();
    }
}
