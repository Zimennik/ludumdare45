﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class ButtonVisual : MonoBehaviour, IEnviromentVisual, IResetable
    {
        [SerializeField] private Material _buttonPressMaterial;
        [SerializeField] private Material _buttonReleaseMaterial;
        [SerializeField] private MeshRenderer _buttonMesh;

        public void Reset()
        {
            _buttonMesh.material = _buttonReleaseMaterial;
        }

        public void VisualActivate()
        {
            Debug.Log("VisualActivate()");
            _buttonMesh.material = _buttonPressMaterial;
            _buttonMesh.transform.localPosition = new Vector3(0f, -1f, 0f);
        }

        public void VisualDeactivate()
        {
            Debug.Log("VisualDeactivate()");
            _buttonMesh.material = _buttonReleaseMaterial;
            _buttonMesh.transform.localPosition = new Vector3(0f, 0f, 0f);
        }
    }
}
