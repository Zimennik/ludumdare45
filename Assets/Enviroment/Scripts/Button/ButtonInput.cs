﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class ButtonInput : EnviromentTriggerAction
    {
        [SerializeField] private bool _flip;
        [SerializeField] private int EarthCountToActivate;

        private GameObject _bufGameObject;
        private Action _activate;
        private Action _deactivate;

        private void Awake()
        {
            SwitchFlip();
        }

        public override void СonditionToActivate(GameObject bufGameObject)
        {
            _bufGameObject = bufGameObject;
            _activate?.Invoke();
        }

        public override void СonditionToDectivate(GameObject bufGameObject)
        {
            _bufGameObject = bufGameObject;
            _deactivate?.Invoke();
        }

        private void SwitchFlip()
        {
            if (_flip)
            {
                _activate = Deactivate;
                //_deactivate = Activate;
            }
            else
            {
                _activate = Activate;
                _deactivate = Deactivate;
            }
        }

        private void Activate()
        {
            bool canActivate = _bufGameObject.GetComponent<Unit.UnitMain>().Mass >=
                EarthCountToActivate;

            if (canActivate)
                ActivateAction();
        }

        private void Deactivate()
        {
            bool canDeactivate = _bufGameObject.GetComponent<Unit.UnitMain>().Mass >=
                EarthCountToActivate;

            if (canDeactivate)
                DeactivateAction();
        }
    }
}
