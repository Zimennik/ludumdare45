﻿using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class Spike : MonoBehaviour
    {
        public Transform respawnPoint;

        [SerializeField] private float _damageValue;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            bool canDamage = collision.gameObject.GetComponent<Unit.UnitMain>().GetStateID() != "SteamCloud";

            if (canDamage)
            {
                Debug.Log("Character is dead");
                collision.transform.position = respawnPoint.position;
                collision.gameObject.GetComponent<IUnit>().TakeDamage(_damageValue);
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            //collision.gameObject.GetComponent<IUnit>().TakeDamage(1f);
        }
    }
}
