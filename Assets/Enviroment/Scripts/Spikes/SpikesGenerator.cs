﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    [ExecuteInEditMode]
    public class SpikesGenerator : MonoBehaviour
    {
        [SerializeField] private int _spikesCount;
        [SerializeField] private GameObject _spikePrefab;
        [SerializeField] private Transform _respawnPoint;

        private List<GameObject> _existingSpikes;

        public void GenerateSpikes()
        {
            ClearSpikes();
            float offsetSpikePosition = 0f;

            for (int i=0;i<_spikesCount;i++)
            {
                Transform spikeTransform = Instantiate(_spikePrefab, transform).transform;
                Spike spike = spikeTransform.GetComponent<Spike>();

                spikeTransform.localPosition = new Vector3(spikeTransform.localScale.x + offsetSpikePosition, 0f, 0f);
                offsetSpikePosition += spikeTransform.localScale.x / 1.5f;
                spike.respawnPoint = _respawnPoint;
            }
        }

        public void ClearSpikes()
        {
            foreach(Transform buf in transform)
            {
                DestroyImmediate(buf.gameObject); 
            }
        }
    }
}
