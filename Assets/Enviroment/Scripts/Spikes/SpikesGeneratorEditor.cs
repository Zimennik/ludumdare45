﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

namespace Enviroment
{
    [CustomEditor(typeof(SpikesGenerator))]
    public class SpikesGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            SpikesGenerator spikesGenerator = (SpikesGenerator)target;

            if (GUILayout.Button("Generate Spikes"))
            {
                spikesGenerator.GenerateSpikes();
            }
            else if (GUILayout.Button("Clear Spikes"))
            {
                spikesGenerator.ClearSpikes();
            }
        }
    }
}
#endif