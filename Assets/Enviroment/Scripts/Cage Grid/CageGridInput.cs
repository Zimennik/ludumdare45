﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class CageGridInput : MonoBehaviour, IResetable
    {
        private Collider2D _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider2D>();
        }

        public void Reset()
        {
            _collider.isTrigger = false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            bool canActivate = collision.gameObject.GetComponent<Unit.UnitMain>().GetStateID() == "SteamCloud";

            if (canActivate)        
                _collider.isTrigger = true;
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            _collider.isTrigger = false;
        }
    }
}
