﻿using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class Enemy : MonoBehaviour
    {
        public Transform respawnPoint;

        [SerializeField] private int _hp;
        [SerializeField] private float _damageValue;

        private void OnTriggerEnter2D(Collider2D collider)
        {
			if(collider.gameObject.GetComponent<Unit.UnitMain>()!=null)
			{
				bool canDamage = collider.gameObject.GetComponent<Unit.UnitMain>().GetStateID() != "SteamCloud";

				if (canDamage)
				{
					Debug.Log("Character is dead");
					collider.transform.position = respawnPoint.position;
					collider.gameObject.GetComponent<IUnit>().TakeDamage(_damageValue);
				}
			}
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            //collision.gameObject.GetComponent<IUnit>().TakeDamage(1f);
        }
    }
}
