﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enviroment
{
    public class MoveLeftRight : MonoBehaviour
    {
        [SerializeField] private bool _needUpAndDown;

        [Header("Vertical")]
        [SerializeField] private float _verticalSpeed;        
        [SerializeField] private float _height;

        [Header("Horizontal")]
        [SerializeField] private float _horizontalSpeed;
        [SerializeField] private float _width;

        void Start()
        {
            StartCoroutine(Move());
        }

        private IEnumerator Move()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                Vector3 pos = transform.localPosition;
               
                float newX = Mathf.Sin(Time.time * _horizontalSpeed) * _width;
                float newY = (_needUpAndDown) ? Mathf.Sin(Time.time * _verticalSpeed) * _height : transform.localPosition.y;

                //if(newX > _width - 0.05f)
                //    transform.localEulerAngles = new Vector3(0f, 180f, 0f);
                //else if(newX < -_width + 0.05f)
                //    transform.localEulerAngles = new Vector3(0f, 0f, 0f);

                transform.localPosition = new Vector3(newX, newY, pos.z);
            }
        }
        
    }
}
