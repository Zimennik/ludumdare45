﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

[CreateAssetMenu(fileName = "ElementsConfig", menuName = "ScriptableObjects/CreateElementsConfig")]
public class ElementsConfig : ScriptableObject
{
    public FireElement FireElement;
    public StoneElement StoneElement;
    public WaterElement WaterElement;
    public AirElement AirElement;
}
