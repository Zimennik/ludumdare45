﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{

    public class SpeedAbility : MonoBehaviour, IAbility
    {
        [SerializeField]
        float _speed;

        public void SetAbilityParamsToPlayer(IUnit unit) {
            unit.Speed += _speed;
            //throw new System.NotImplementedException();
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
