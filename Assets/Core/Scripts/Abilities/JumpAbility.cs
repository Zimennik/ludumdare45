﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class JumpAbility : MonoBehaviour,IAbility
    {
        [SerializeField]
        float _jumpForce;
        public void SetAbilityParamsToPlayer(IUnit unit) {
            unit.JumpForce += _jumpForce;
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}