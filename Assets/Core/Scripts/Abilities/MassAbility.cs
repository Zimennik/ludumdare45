﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{

    public class MassAbility : MonoBehaviour, IAbility
    {

        [SerializeField]
        float _mass;

        public void SetAbilityParamsToPlayer(IUnit unit) {
            unit.Mass += _mass;
            //throw new System.NotImplementedException();
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}