﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class FireAbility : MonoBehaviour,IAbility
    {
        [SerializeField]
        float _fireDistance;
        public void SetAbilityParamsToPlayer(IUnit unit) {
            unit.AttackLevel += _fireDistance;
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}