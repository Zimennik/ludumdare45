﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class StoneElement : MonoBehaviour,IElement
    {

        [SerializeField]
        MassAbility _massAbility;
        [SerializeField]
        Sprite _stoneSprite;

        public Sprite Sprite { get => _stoneSprite; set => _stoneSprite = value; }
        public IAbility Ability { get => _massAbility; set => _massAbility = (MassAbility)value; }
    

        // Start is called before the first frame update
        void Start() {
          //  Ability = _massAbility;
        }
        // Update is called once per frame
        void Update() {

        }

        public void SetElementToPLayer(IUnit unit) {
            Ability.SetAbilityParamsToPlayer(unit);
        }
    }
}