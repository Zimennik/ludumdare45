﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Core {
    public interface IUnit {
        float Speed { get; set; }
        float Mass { get; set; }
        float JumpForce { get; set; }
        float AttackLevel { get; set; }
        float Health { get; set; }

        void TakeDamage(float damageValue);
        void Teleport(Vector2 pos);
        void Death();
    }

    public interface IUnitState {
        void Move();
        void CustomUpdateFirst();
        void Attack();
        void TakeDamage(float damageValue);
        void Teleport(Vector2 pos);
        void Death();
    }

    public interface IAbility {

        void SetAbilityParamsToPlayer(IUnit unit);
    }

    public interface IElement{

        IAbility Ability { get; set; }
        Sprite Sprite { get; set; }
        void SetElementToPLayer(IUnit unit);
        
    }
}
