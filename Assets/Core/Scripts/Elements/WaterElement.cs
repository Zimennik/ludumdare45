﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class WaterElement : MonoBehaviour, IElement
    {

        [SerializeField]
        JumpAbility _jumpforce;
        [SerializeField]
        Sprite _waterSprite;

        public Sprite Sprite { get => _waterSprite; set => _waterSprite = value; }

        public IAbility Ability { get => _jumpforce; set => _jumpforce = (JumpAbility)value; }

        // Start is called before the first frame update
        void Start() {
            Ability = _jumpforce;
        }
        // Update is called once per frame
        void Update() {

        }

        public void SetElementToPLayer(IUnit unit) {
            Ability.SetAbilityParamsToPlayer(unit);
        }
    }
}

