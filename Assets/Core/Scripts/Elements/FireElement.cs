﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class FireElement : MonoBehaviour, IElement
    {
        [SerializeField]
        FireAbility _fireDistance;

        [SerializeField]
        Sprite _fireSprite;

        public Sprite Sprite { get => _fireSprite; set => _fireSprite = value; }
        public IAbility Ability { get => _fireDistance; set => _fireDistance = (FireAbility)value; }

        // Start is called before the first frame update
        void Start() {
            Ability = _fireDistance;
        }
        // Update is called once per frame
        void Update() {

        }

        public void SetElementToPLayer(IUnit unit) {
            Ability.SetAbilityParamsToPlayer(unit);
        }
    }
}