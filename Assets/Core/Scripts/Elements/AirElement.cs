﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class AirElement : MonoBehaviour, IElement
    {

        [SerializeField]
        SpeedAbility _speed;

        [SerializeField]
        Sprite _airSprite;

        public Sprite Sprite { get => _airSprite; set => _airSprite = value; }
        public IAbility Ability { get => _speed; set => _speed = (SpeedAbility)value; }

        // Start is called before the first frame update
        void Start() {
          //  Ability = _fireDistance;
        }
        // Update is called once per frame
        void Update() {

        }

        public void SetElementToPLayer(IUnit unit) {
            Debug.Log(unit);
            Debug.Log(Ability);
            Debug.Log(gameObject.name);
            Ability.SetAbilityParamsToPlayer(unit);
        }
    }
}
