﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidualStatesTest : MonoBehaviour
{

    [SerializeField]
    GameObject _defaultModel;
    [SerializeField]
    GameObject _SteamCloudObject;
    [SerializeField]
    GameObject _MeteorObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDefaultState() {
        _SteamCloudObject.SetActive(false);
        _MeteorObject.SetActive(false);
        _defaultModel.SetActive(true);
    }
    public void SetCloudState() {
        SetDefaultState();
        _defaultModel.SetActive(false);
        _SteamCloudObject.SetActive(true);
    }

    public void SetMeteorStat() {
        SetDefaultState();
        _defaultModel.SetActive(false);
        _MeteorObject.SetActive(true);
    }
}
