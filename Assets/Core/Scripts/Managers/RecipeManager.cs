﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Core {

    [System.Serializable]
   public class Recipe {
        [SerializeField]
        public string stateId;

        [SerializeField]
        public List<StoneElement> _stoneElements;
        [SerializeField]
        public List<FireElement> _fireElements;
        [SerializeField]
        public List<AirElement> _airElements;
        [SerializeField]
        public List<WaterElement> _waterElements;

        private List<IElement> recipeElements;

      //  public List<IElement> RecipeElements { get => (_stoneElements.Concat(_fireElements)); set => recipeElements = value; }

        public List<IElement> GetRecipeElements() {
            var allelements = new List<IElement>(_stoneElements.Count +
                                    _fireElements.Count +
                                    _airElements.Count+
                                    _waterElements.Count);
            allelements.AddRange(_stoneElements);
            allelements.AddRange(_fireElements);
            allelements.AddRange(_airElements);
            allelements.AddRange(_waterElements);
            return allelements;
        }
    }
    public class RecipeManager : MonoBehaviour
    {
        [SerializeField]
        public List<Recipe> _recipes;

        //[SerializeField]
        //Recipe _meteorRecipe;
        //[SerializeField]
        //Recipe _smash;
        //[SerializeField]
        //Recipe _steamCloud;
        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
