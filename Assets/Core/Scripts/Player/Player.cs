﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Core
{
    public class Player : MonoBehaviour, IUnit
    {
        public float Speed { get ; set ; }
        public float Mass { get; set; }
        public float AttackLevel { get; set; }
        public float JumpForce { get ; set; }
        public float Health { get; set; }

        [SerializeField]
        ElementsConfig _elementsConfig;
        [SerializeField]
        RecipeManager _recipeManager;

        StoneElement _stoneElement;
        AirElement  _airElement;
        WaterElement  _waterElement;
        FireElement _fireElement;


        [SerializeField]
        Text stateText;

        List<IElement> _currentElements=new List<IElement>();

        IUnitState _currentstate;
        // Start is called before the first frame update
        void Start() {
            //_currentstate = new StateFlame(0f,gameObject,1f,10f);
        }

        // Update is called once per frame
        void Update()
        {
            _currentstate.Move();

            if(Input.GetKeyDown(KeyCode.Q)) {
                AddElement(_elementsConfig.StoneElement);
            }
            if(Input.GetKeyDown(KeyCode.W)) {
                AddElement(_elementsConfig.AirElement);
            }
            if(Input.GetKeyDown(KeyCode.E)) {
                AddElement(_elementsConfig.FireElement);
            }
            if(Input.GetKeyDown(KeyCode.R)) {
                AddElement(_elementsConfig.WaterElement);
            }

        }

        void AddElement(IElement element) {
            if(_currentElements.Count>3) {
                return;
            }
            _currentElements.Add(element);

          
            CheckCombine();

            element.SetElementToPLayer(this);
            RefreshUI();
        }

        void RefreshUI() {
            stateText.text = Speed.ToString() + Mass.ToString() + JumpForce.ToString() + AttackLevel.ToString();
        }

        void CheckCombine() {
            _recipeManager._recipes.ForEach(x =>Debug.Log(x.GetRecipeElements().Count));

            _recipeManager._recipes.ForEach(x=> {
                var list1 = _currentElements;
                var list2 = x.GetRecipeElements();
                if((x.GetRecipeElements().Count() == _currentElements.Count())) {
                    var count = 0;

                    for(int k = 0; k <= list1.Count-1; k++) {

                        if(list1.FirstOrDefault(_ => string.Equals(list2[k].GetType(), _.GetType())) != null) {
                            count++;
                        }
                    }
                    if(count == list2.Count) {
                        Debug.LogFormat("<color=#00ff00ff> Equal!!! </color>");
                        SetSpacialAbility(x.stateId);
                        Debug.Log(x.stateId);
                    }
                }
            });
        }

        void SetSpacialAbility(string stateID) {
            switch(stateID) {
                case "Meteor":
                   // _currentstate = new MeteorState();
                    break;
                case "SteamCloud":
                   // _currentstate = new SteamCloudState();
                    break;
                case "Smash":
                    //_currentstate = new SmashState();
                    break;
            }
        }

        public void TakeDamage(float damageValue)
        {
            throw new System.NotImplementedException();
        }

        public void Teleport(Vector2 pos)
        {
            throw new System.NotImplementedException();
        }

        public void Death()
        {
            throw new System.NotImplementedException();
        }
    }
}
