﻿using System.Collections;
using System.Collections.Generic;
using Unit;
using UnityEngine;

namespace Core {
    public class StateFlame : IUnitState
    {
        public StateFlame(UnitMain _mUnit, Rigidbody2D mRB)
        {
            _unitMain = _mUnit;
            _mRigidbody2D = mRB;
            _mRigidbody2D.gravityScale = 1f;
        }

        #region Local values

        private Rigidbody2D _mRigidbody2D;
        private UnitMain _unitMain;

        private bool _tryMove;
        private float _movementSideMove;

        #endregion

        public void Attack() {
        }

        public void Move() {

            float inertiaMult = (1.0f - 0.7f * (_unitMain.Mass / 4.0f));

            if (Input.GetKey(KeyCode.RightArrow))
            {
                _tryMove = true;
                if (Mathf.Sign(_movementSideMove) < 0)
                {
                    _movementSideMove = 0;
                }
                _movementSideMove = _movementSideMove + Mathf.Clamp(1 - _movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult,
                                        _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult);
                _unitMain.LookToSide(true);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                _tryMove = true;
                if (Mathf.Sign(_movementSideMove) > 0)
                {
                    _movementSideMove = 0;
                }
                _movementSideMove = _movementSideMove + Mathf.Clamp(-1 - _movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult,
                                        _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult);
                _unitMain.LookToSide(false);
            }
        }

        public void Jump() {
            throw new System.NotImplementedException();
        }

        public void TakeDamage(float damageValue)
        {

        }

        public void Teleport(Vector2 pos)
        {

        }

        public void Death()
        {
            throw new System.NotImplementedException();
        }

        public void CustomUpdateFirst()
        {
            int returnY = _unitMain.GetOnFloor() ? 0 : 1;

            float multSpeed = 1;


            multSpeed = _movementSideMove * (multSpeed + Mathf.Clamp(_unitMain.GetCalibrateStatsBundle().SpeedAdd - 1, -1f, _unitMain.GetCalibrateStatsBundle().SpeedAdd)) * _unitMain.GetCalibrateStatsBundle().SpeedMult;

            if (!_unitMain.GetOnFloor() && (Mathf.Sign(multSpeed) * Mathf.Sign(_mRigidbody2D.velocity.x) > 0 || Mathf.Abs(_movementSideMove) < 0.1f))
            {
                if (Mathf.Abs(multSpeed) < Mathf.Abs(_mRigidbody2D.velocity.x))
                {
                    multSpeed = _mRigidbody2D.velocity.x;
                }
            }

            _mRigidbody2D.velocity = Vector2.up * _mRigidbody2D.velocity.y * returnY + Vector2.right * multSpeed * 0.6f;


            if (_mRigidbody2D.velocity.y < 0)
            {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * 1.5f * Time.deltaTime * Time.timeScale;
            }
            else if (_mRigidbody2D.velocity.y > 0)
            {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime * Time.timeScale;
            }

            if (!_tryMove)
            {
                float inertiaMult = 1;
                _movementSideMove = _movementSideMove + Mathf.Clamp(-_movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult,
                                        _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult);


            }
            else
            {
                _tryMove = false;
            }
        }
    }
}
