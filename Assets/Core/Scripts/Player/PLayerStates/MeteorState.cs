﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit;

namespace Core
{
    public class MeteorState : IUnitState
    {

        private Rigidbody2D _mRigidbody2D;
        private UnitMain _unitMain;

        float speed;

        private enum direction { left, right }
        private direction moveDiection = direction.left;

        private GameObject gameObject;

        public MeteorState(UnitMain unitMain) {
            _unitMain = unitMain;
            _mRigidbody2D = _unitMain.GetComponent<Rigidbody2D>();
            _mRigidbody2D.gravityScale = 0f;
            _mRigidbody2D.velocity = Vector2.zero;
            _unitMain.transform.position = new Vector3(_unitMain.transform.position.x, _unitMain.transform.position.y+0.1f, _unitMain.transform.position.z) ;
            AudioManager.AudioManager.Instance.PlaySoundMainCharacter("Meteor");
        }

        public void Attack() {
     
        }

        public void Move() {
            _mRigidbody2D.velocity = Vector2.zero;
            Vector3 position = _unitMain.transform.position;
            float newX = position.x;

            if(_unitMain.transform.rotation.eulerAngles.y == 0) {
                moveDiection = direction.right;
            } else {
                moveDiection = direction.left;
            }

            switch(moveDiection) {
                case direction.left:
                    newX = position.x - Time.deltaTime *15.0f;
                        position.x = newX;
                    break;
                case direction.right:
                    newX = position.x + Time.deltaTime *15.0f;
                    position.x = newX;
                    break;
            }

            _unitMain.transform.position = position;
        }

        public void Jump() {
            throw new System.NotImplementedException();
        }

        public void TakeDamage(float damageValue)
        {

        }

        public void Teleport(Vector2 pos)
        {

        }

        public void Death()
        {
            throw new System.NotImplementedException();
        }

        public void CustomUpdateFirst()
        {

        }
    }
}


