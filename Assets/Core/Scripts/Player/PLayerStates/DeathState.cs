﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class DeathState : IUnitState
    {
        private float _attackTime;
        private float timeBetweenAttacks = 0.5f;
        private float movementSpeed = 2f;

        private enum direction { left, right }
        private direction moveDiection = direction.left;

        private GameObject gameObject;
        private float minX, maxX;

        public string StateID { get; set; }

        public DeathState(float startTime, GameObject gameObject, float minX, float maxX) {
         
        }

        public void Attack() {
            throw new System.NotImplementedException();
        }

        public void Move() {
            throw new System.NotImplementedException();
        }

        public void Jump() {
            throw new System.NotImplementedException();
        }

        public void TakeDamage(float damageValue)
        {

        }

        public void Teleport(Vector2 pos)
        {

        }

        public void Death()
        {
            throw new System.NotImplementedException();
        }

        public void CustomUpdateFirst()
        {

        }
    }
}


