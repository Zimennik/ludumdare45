﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class DefaultState : IUnitState
    {
        private float movementSpeed = 2f;

        private enum direction { left, right }
        private direction moveDiection = direction.left; 
         
        private GameObject gameObject;
        private float minX, maxX;

        public DefaultState(IUnit unit, float startTime, GameObject gameObject, float minX, float maxX) {
            this.gameObject = gameObject;
            this.minX = minX;
            this.maxX = maxX;
        }

        public void Attack() {
       
        }

        public void Move() {
            Vector3 position = gameObject.transform.position;
            float newX = position.y;

            switch(moveDiection) {
                case direction.left:
                    newX = position.y - Time.deltaTime * movementSpeed;
                    if(newX < minX) {
                        moveDiection = direction.right;
                    } else {
                        position.y = newX;
                    }
                    break;
                case direction.right:
                    newX = position.y + Time.deltaTime * movementSpeed;
                    if(newX > maxX) {
                        moveDiection = direction.left;
                    } else {
                        position.y = newX;
                    }
                    break;
            }

            gameObject.transform.position = position;
        }

        public void Jump() {
            //throw new System.NotImplementedException();
        }

        public void TakeDamage(float damageValue)
        {

        }

        public void Teleport(Vector2 pos)
        {

        }

        public void Death()
        {
            //throw new System.NotImplementedException();
        }

        public void CustomUpdateFirst()
        {
            
        }
    }
}

