﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit;
using System.Diagnostics;


namespace Core
{
    public class SmashState : IUnitState
    {
        Stopwatch _timer=new Stopwatch();

        private Rigidbody2D _mRigidbody2D;
        private UnitMain _unitMain;

        bool startJump;
        public SmashState(UnitMain unit) {
            _unitMain = unit;
            _mRigidbody2D = _unitMain.GetComponent<Rigidbody2D>();
            startJump = true;
        }

        public void Attack() {
            // throw new System.NotImplementedException();
        }

        public void Move() {
            if(startJump) {
                AudioManager.AudioManager.Instance.PlaySoundMainCharacter("Smash");
                _timer.Start();
                UnityEngine.Debug.LogFormat("<color=#00ff00ff> AddForce </color>");
                _mRigidbody2D.AddForce(Vector2.up*15f,ForceMode2D.Impulse);
                startJump = false;
            }

            if(_timer.ElapsedMilliseconds>450f) {

                UnityEngine.Debug.LogFormat("<color=#00ff00ff> BOOOOOOM </color>");
                _timer.Stop();
                _timer.Reset();
                UnityEngine.Debug.LogFormat("<color=#00ff00ff> AddForce </color>");
                _mRigidbody2D.AddForce(Vector2.down * 30f, ForceMode2D.Impulse);
                //startJump = false;
            }
         //   throw new System.NotImplementedException();
        }

        public void Jump() {
          //  throw new System.NotImplementedException();
        }

        public void TakeDamage(float damageValue)
        {

        }

        public void Teleport(Vector2 pos)
        {

        }

        public void Death()
        {
          //  throw new System.NotImplementedException();
        }

        public void CustomUpdateFirst()
        {

        }
    }
}




