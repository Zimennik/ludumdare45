﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unit;


namespace Core
{
    public class SteamCloudState : IUnitState
    {
        private Rigidbody2D _mRigidbody2D;
        private UnitMain _unitMain;

       // private bool _tryJump;
        private float _movementSideMove;



        public SteamCloudState(UnitMain unit) {
            _unitMain = unit;
            _mRigidbody2D = unit.GetComponent<Rigidbody2D>();
        }
     


        public void Move() {
          
            if(Input.GetKey(KeyCode.RightArrow)) {
                _movementSideMove = _movementSideMove + Mathf.Clamp(1 - _movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime, _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime);
                _unitMain.LookToSide(true);
            } else if(Input.GetKey(KeyCode.LeftArrow)) {
                _movementSideMove = _movementSideMove + Mathf.Clamp(-1 - _movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime, _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime);
                _unitMain.LookToSide(false);
            } else {

                _movementSideMove = _movementSideMove + Mathf.Clamp(-_movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime, _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime);
            }



        }

        public void Attack() {
          //  throw new System.NotImplementedException();
        }

        public void Jump() {
          //  throw new System.NotImplementedException();
        }

        public void TakeDamage(float damageValue)
        {

        }

        public void Teleport(Vector2 pos)
        {

        }

        public void Death()
        {
           // throw new System.NotImplementedException();
        }

        public void CustomUpdateFirst()
        {
            int returnY = _unitMain.GetOnFloor() ? 0 : 1;

            float multSpeed = (1 + _unitMain.Speed - _unitMain.Mass);
            if(multSpeed < 1) {
                multSpeed = 1.0f / (_unitMain.Mass);
            }

            multSpeed = _movementSideMove * (multSpeed + Mathf.Clamp(_unitMain.GetCalibrateStatsBundle().SpeedAdd - 1, -1f, _unitMain.GetCalibrateStatsBundle().SpeedAdd)) * _unitMain.GetCalibrateStatsBundle().SpeedMult;

            if(!_unitMain.GetOnFloor() && (Mathf.Sign(multSpeed) * Mathf.Sign(_mRigidbody2D.velocity.x) > 0 || Mathf.Abs(_movementSideMove) < 0.1f)) {
                if(Mathf.Abs(multSpeed) < Mathf.Abs(_mRigidbody2D.velocity.x)) {
                    multSpeed = _mRigidbody2D.velocity.x;
                }
            }

            _mRigidbody2D.velocity = Vector2.up * _mRigidbody2D.velocity.y * returnY + Vector2.right * multSpeed;


            if(_mRigidbody2D.velocity.y < 0) {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * 1.5f * Time.deltaTime;
            } else if(_mRigidbody2D.velocity.y > 0 ) {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            }

           // _tryJump = false;
        }
    }
}




