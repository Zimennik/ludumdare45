﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Unit.VisualState
{
    [Serializable]
    public class UnitVisualStateData
    {
        public Animator MainAnimator;
        public GameObject VisualObject;
        public ParticleSystem[] ParticleSystems;

        public void SetActive(bool state)
        {
            if (state)
            {
                //MainAnimator???
                VisualObject?.SetActive(true);
                foreach (var part in ParticleSystems)
                {
                    Debug.Log("wefwefw");
                    var emiss = part.emission;
                    emiss.rateOverTimeMultiplier = 1;
                    emiss.rateOverDistanceMultiplier = 1;
                }
            }
            else
            {
                VisualObject?.SetActive(false);
                foreach (var part in ParticleSystems)
                {
                    var emiss = part.emission;
                    emiss.rateOverTimeMultiplier = 0;
                    emiss.rateOverDistanceMultiplier = 0;
                }
            }
        }
    }
}

