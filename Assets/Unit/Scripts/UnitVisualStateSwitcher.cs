﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unit.VisualState
{
    public class UnitVisualStateSwitcher : MonoBehaviour
    {
        [Serializable]
        public struct VisualState
        {
            public string StateID;
            public UnitVisualStateData MainVisualData;
        }

        [SerializeField] private VisualState[] _listOfVisualsInspector;
        private Dictionary<string, UnitVisualStateData> _listOfVisuals;


        private UnitVisualStateData _lastVisual;

        // Start is called before the first frame update
        void Start()
        {
            CheckList();
        }

        private bool CheckList()
        {
            if (_listOfVisuals == null)
            {
                _listOfVisuals = new Dictionary<string, UnitVisualStateData>();

                foreach (var data in _listOfVisualsInspector)
                {
                    _listOfVisuals.Add(data.StateID, data.MainVisualData);
                }

                return false;
            }
            else
            {


                return true;
            }
        }

        public void SetState(string StateID)
        {
            CheckList();

            _lastVisual?.SetActive(false);
            if (_listOfVisuals.ContainsKey(StateID))
            {
                _listOfVisuals[StateID].SetActive(true);
                _lastVisual = _listOfVisuals[StateID];
            }
            else
            {
                Debug.Log($"<color=red> '{StateID}' state not found in UnitVisualStateSwitcher </color>");
            }
        }

        public Animator GetAnimatorOfState(string StateID)
        {
            if (_listOfVisuals.ContainsKey(StateID))
            {
                return _listOfVisuals[StateID].MainAnimator;
            }
            else
            {
                Debug.Log($"<color=red> '{StateID}' state not found in UnitVisualStateSwitcher </color>");
                return null;
            }
        }
    }
}
