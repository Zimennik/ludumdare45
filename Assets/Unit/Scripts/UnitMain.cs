﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using Managers;
using Unit.Bundles;
using Unit.State;
using Unit.VisualState;
using UnityEngine;

namespace Unit
{
    public class UnitMain : MonoBehaviour, IUnit
    {
        private float _slowmoutionMult = 1;
        private bool _onFloor;

        #region Inspector

        [SerializeField] Transform _STEAM_CLOUD;

        [SerializeField] UIController _uiManager;

        [SerializeField] ElementsConfig _elementsConfig;
        [SerializeField] RecipeManager _recipeManager;

        [SerializeField] private Rigidbody2D _mainRigidbody2D;
        [SerializeField] private GameObject _attackPrefab;
        [SerializeField] private UnitVisualStateSwitcher _unitVisualStateSwitcher;
        [SerializeField] private Animator _mainAnimator;

        #endregion

        #region Stats

        public float Speed { get; set; }
        public float Mass { get; set; }
        public float JumpForce { get; set; }
        public float AttackLevel { get; set; }
        public float Health { get; set; }

        #endregion

        #region Calibrate stats

        [SerializeField] CalibrateStatsBundle _mainConstantStats;

        #endregion

        #region ElementsLogic

        int _maxElements;
        List<IElement> _currentElements = new List<IElement>();
        IUnitState _currentstate;
        private string _stateID;

        bool AddElement(IElement element)
        {
            if (_currentElements.Count > _maxElements - 1)
            {
                return false;
            }

            _currentElements.Add(element);


            //CheckCombine();

            element.SetElementToPLayer(this);

            return true;
        }

        public void AddEmptyElement(int count = 1)
        {
            if (_maxElements == 0)
            {
                SetSpacialAbility("Default");
                _uiManager.ShowAllUI();
            }

            _maxElements += count;
            _uiManager._runeSelector.SetRuneCount(_maxElements);
        }

        public int GetEmptyElements()
        {
            return _maxElements;
        }

        public string GetStateID()
        {
            return _stateID;
        }

        void CheckCombine()
        {
            _recipeManager._recipes.ForEach(x => Debug.Log(x.GetRecipeElements().Count));
            string stateID = string.Empty;
            _recipeManager._recipes.ForEach(x =>
            {
                var list1 = _currentElements;
                var list2 = x.GetRecipeElements();
                if ((x.GetRecipeElements().Count() == _currentElements.Count()))
                {
                    var count = 0;
                    for (int k = 0; k <= list1.Count - 1; k++)
                    {
                        if (list1.FirstOrDefault(_ => string.Equals(list2[k].GetType(), _.GetType())) != null)
                        {
                            count++;
                        }
                    }

                    if (count == list2.Count)
                    {
                        Debug.LogFormat("<color=#00ff00ff> Equal!!! </color>");
                        stateID = x.stateId;

                        Debug.Log(x.stateId);
                    }
                    else
                    {
                        //  SetSpacialAbility("");
                    }
                }
                else
                {
                    //  SetSpacialAbility("");
                }
            });
            SetSpacialAbility(stateID);
        }

        public void SetSpacialAbility(string stateID = "Default")
        {
            _stateID = stateID;
            switch (stateID)
            {
                case "Flame":
                    _unitVisualStateSwitcher.SetState(stateID);
                    _currentstate = new StateFlame(this, _mainRigidbody2D);
                    break;
                case "Default":
                    if (_currentstate.GetType() != typeof(StateDefault))
                    {
                        _unitVisualStateSwitcher.SetState(stateID);
                        _currentstate = new StateDefault(this, _mainRigidbody2D, _mainAnimator);
                    }

                    break;
                case "Disassembled":
                    //_currentstate = new StateDisassembled(this);
                    break;
                case "Meteor":
                    _unitVisualStateSwitcher.SetState(stateID);
                    _currentstate = new MeteorState(this);
                    break;
                case "SteamCloud":
                    Debug.LogFormat("<color=#00ff00ff> SteamCloud </color>");
                    _unitVisualStateSwitcher.SetState(stateID);
                    _currentstate = new SteamCloudState(this);
                    StartCoroutine(DelayState("", 2f));
                    break;
                case "Smash":
                    _unitVisualStateSwitcher.SetState(stateID);
                    _currentstate = new SmashState(this);
                    StartCoroutine(DelayState("", 1.1f));
                    break;
                default:
                    if (_currentstate.GetType() != typeof(StateDefault))
                    {
                        _stateID = "Default";
                        _unitVisualStateSwitcher.SetState("Default");
                        _currentstate = new StateDefault(this, _mainRigidbody2D, _mainAnimator);
                    }

                    break;
            }
        }


        IEnumerator DelayState(string statID, float delay)
        {
            yield return new WaitForSeconds(delay);
            SetSpacialAbility(statID);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            // Debug.LogFormat("<color=#00ff00ff> CollisionEnter </color>");
            if (_currentstate.GetType() == typeof(MeteorState))
            {
                Debug.LogFormat("<color=#00ff00ff> CollisionEnter </color>");
                SetSpacialAbility("");
            }
        }

        #endregion

        #region Get's Set's methods

        public void SetOnFloor(bool onFloor)
        {
            _onFloor = onFloor;
        }

        public bool GetOnFloor()
        {
            return _onFloor;
        }

        public GameObject GetAttackPrefab()
        {
            return _attackPrefab;
        }

        public CalibrateStatsBundle GetCalibrateStatsBundle()
        {
            return _mainConstantStats;
        }

        #endregion

        public void LookToSide(bool right)
        {
            transform.rotation = Quaternion.Euler(0, right ? 0 : 180, 0);
            _STEAM_CLOUD.rotation = Quaternion.Euler(0, right ? 0 : 180, 0);
        }

        void Start()
        {
            //AddEmptyElement(4);
            //Debug.Log("<color=#ff0000> ADDED 4 EMPTY ELEMENT </color>");


            LookToSide(true);
            //_currentstate = new StateFlame(10, gameObject, 0, 4);
            SetSpacialAbility("Flame");
            _uiManager.HideAllUI();
            //_currentstate = new StateDefault(this,_mainRigidbody2D);
        }


        #region Interface Methods / Update

        // Update is called once per frame
        void Update()
        {
            //AddElement(_elementsConfig.StoneElement);

            _currentstate?.CustomUpdateFirst();

            if (_stateID == "Default" && Input.GetKeyDown(KeyCode.Space))
            {
                _currentElements.Clear();
                _currentElements.TrimExcess();
            }

            if ((_stateID == "Default" || _stateID == "Disassembled") && Input.GetKey(KeyCode.Space))
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Speed = 0;
                    JumpForce = 0;
                    Mass = 0;
                    AttackLevel = 0;

                    SetSpacialAbility("Disassembled");
                    _uiManager._runeSelector.ResetAll();
                    _uiManager.ShowRuneSelect();
                }

                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if (AddElement(_elementsConfig.WaterElement))
                    {
                        _uiManager._runeSelector.SetRune("water", _currentElements.Count - 1);
                    }
                }

                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    if (AddElement(_elementsConfig.FireElement))
                    {
                        _uiManager._runeSelector.SetRune("fire", _currentElements.Count - 1);
                    }
                }

                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (AddElement(_elementsConfig.StoneElement))
                    {
                        _uiManager._runeSelector.SetRune("stone", _currentElements.Count - 1);
                    }
                }

                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    if (AddElement(_elementsConfig.AirElement))
                    {
                        _uiManager._runeSelector.SetRune("air", _currentElements.Count - 1);
                    }
                }

                _slowmoutionMult -= Time.deltaTime * 3.5f;

                if (_slowmoutionMult < 0)
                {
                    _slowmoutionMult = 0;
                }

                TimeManager.SetTimeMult(0.1f + _slowmoutionMult * 0.9f);
            }
            else
            {
                if (_slowmoutionMult < 1)
                {
                    _slowmoutionMult += Time.deltaTime * 5.5f;

                    if (_slowmoutionMult > 1)
                    {
                        _slowmoutionMult = 1;
                    }

                    TimeManager.SetTimeMult(0.1f + _slowmoutionMult * 0.9f);
                }

                _currentstate?.Move();

                if (Input.GetKeyDown(KeyCode.X))
                {
                    _currentstate?.Attack();
                }
            }


            if ((_stateID != "Default" && _stateID != "Flame") && Input.GetKeyUp(KeyCode.Space))
            {
                _uiManager.HideRuneSelect();
                SetSpacialAbility();
            }

            if (_stateID == "Default" && Input.GetKeyDown(KeyCode.C))
            {
                CheckCombine();
            }
        }

        public void Death()
        {
            _currentstate.Death();
        }

        public void TakeDamage(float damageValue)
        {
            _currentstate.TakeDamage(damageValue);
        }

        public void Teleport(Vector2 pos)
        {
            _currentstate.Teleport(pos);
        }

        #endregion
    }
}