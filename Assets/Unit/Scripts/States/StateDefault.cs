﻿using System.Collections;
using System.Collections.Generic;
using Core;
using UnityEngine;

namespace Unit.State
{
    public class StateDefault : IUnitState
    {
        public StateDefault(UnitMain _mUnit, Rigidbody2D mRB, Animator animator)
        {
            _unitMain = _mUnit;
            _mRigidbody2D = mRB;
            _mRigidbody2D.gravityScale = 1f;
            _animator = animator;
        }

        #region Local values

        private Rigidbody2D _mRigidbody2D;
        private UnitMain _unitMain;
        private Animator _animator;

        private bool _tryJump;
        private bool _tryMove;
        private float _movementSideMove;

        private float _attackCD = 1;
        private float _lastAttack = 0;

        private float _lastVelocityY;

        #endregion

        #region Interface logic

        public void Move()
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                _tryJump = true;
                //Debug.Log($"{_onFloor} {_jumpCounts} {_mainConstantStats.MaxJumps}" );
                if (_unitMain.GetOnFloor())
                {
                    _unitMain.SetOnFloor(false);
                    //h = s^2/20 >>> s = sqrt(20h)
                    //
                    //Debug.Log(_mRigidbody2D.velocity +  " tttt");
                    _mRigidbody2D.velocity = new Vector2(_mRigidbody2D.velocity.x,
                        (Mathf.Sqrt(Mathf.Clamp(_unitMain.JumpForce - _unitMain.Mass*0.5f, _unitMain.JumpForce > 0 ? 0.25f:0, 4) * 20 *
                                    _unitMain.GetCalibrateStatsBundle().JumpMult)));
                    //Debug.Log(_mRigidbody2D.velocity);
                    if (_unitMain.JumpForce > 0)
                    {
                        _animator.SetTrigger("jumpStart");
                    }
                }
            }

            if (Input.GetKey(KeyCode.Z))
            {
                _tryJump = true;
            }

            float inertiaMult = (1.0f - 0.7f * (_unitMain.Mass / 4.0f));

            if (Input.GetKey(KeyCode.RightArrow))
            {
                _tryMove = true;
                if (Mathf.Sign(_movementSideMove) < 0)
                {
                    _movementSideMove = 0;
                }
                _movementSideMove = _movementSideMove + Mathf.Clamp(1 - _movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult,
                                        _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult);
                _unitMain.LookToSide(true);
            }
            else if(Input.GetKey(KeyCode.LeftArrow))
            {
                _tryMove = true;
                if (Mathf.Sign(_movementSideMove) > 0)
                {
                    _movementSideMove = 0;
                }
                _movementSideMove = _movementSideMove + Mathf.Clamp(-1 - _movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult,
                                        _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult);
                _unitMain.LookToSide(false);
            }

            _animator.SetFloat("horizontalSpeed", Mathf.Abs(_mRigidbody2D.velocity.x));
            _animator.SetFloat("verticalSpeed", _mRigidbody2D.velocity.y);
            _lastVelocityY = _mRigidbody2D.velocity.y;

            _animator.SetBool("boolLanded", _unitMain.GetOnFloor());
        }

        public void Attack()
        {
            if (Time.time -_lastAttack > _attackCD)
            {
                _lastAttack = Time.time;
                CreateFireBall();
            }

            
        }

        private void CreateFireBall()
        {
            GameObject fireBall = GameObject.Instantiate(_unitMain.GetAttackPrefab()) as GameObject;

            int side = _unitMain.transform.rotation.eulerAngles.y > 90 ? -1 : 1;

            fireBall.transform.position = _unitMain.transform.position + Vector3.right * side + Vector3.up * 0.5f;
            fireBall.GetComponent<Fireball>().InitFireball(side, _unitMain.AttackLevel*0.95f);
        }

        public void TakeDamage(float damageValue)
        {
            _unitMain.Health -= damageValue;
            if (_unitMain.Health < 0)
            {
                Death();
            }
            if (_unitMain.Health > _unitMain.GetCalibrateStatsBundle().MaxHealth)
            {
                _unitMain.Health = _unitMain.GetCalibrateStatsBundle().MaxHealth;
            }
        }

        public void Teleport(Vector2 pos)
        {
            _unitMain.transform.position = pos;
        }

        public void Death()
        {

        }

        public void CustomUpdateFirst()
        {
            int returnY = _unitMain.GetOnFloor() ? 0 : 1;
            
            float multSpeed = Mathf.Clamp((1 + _unitMain.Speed - _unitMain.Mass*0.5f),0.25f, 1 + _unitMain.Speed);
            

            multSpeed = _movementSideMove * (multSpeed + Mathf.Clamp(_unitMain.GetCalibrateStatsBundle().SpeedAdd - 1, -1f, _unitMain.GetCalibrateStatsBundle().SpeedAdd)) * _unitMain.GetCalibrateStatsBundle().SpeedMult;

            if (!_unitMain.GetOnFloor() && (Mathf.Sign(multSpeed) * Mathf.Sign(_mRigidbody2D.velocity.x) > 0 || Mathf.Abs(_movementSideMove) < 0.1f))
            {
                if (Mathf.Abs(multSpeed) < Mathf.Abs(_mRigidbody2D.velocity.x))
                {
                    multSpeed = _mRigidbody2D.velocity.x;
                }
            }

            _mRigidbody2D.velocity = Vector2.up * _mRigidbody2D.velocity.y * returnY + Vector2.right * multSpeed;


            if (_mRigidbody2D.velocity.y < 0)
            {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * 1.5f * Time.deltaTime * Time.timeScale;
            }
            else if (_mRigidbody2D.velocity.y > 0 && !_tryJump)
            {
                _mRigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime * Time.timeScale;
            }

            if(!_tryMove)
            {
                float inertiaMult = (1.0f - 0.7f * (_unitMain.Mass / 4.0f));
                _movementSideMove = _movementSideMove + Mathf.Clamp(-_movementSideMove, -_unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult,
                                        _unitMain.GetCalibrateStatsBundle().AccelerationViaController * Time.deltaTime * Time.timeScale * inertiaMult);


            }
            else
            {
                _tryMove = false;
            }

            _tryJump = false;
        }

        #endregion

    }
}
